#!/bin/bash

SH=$(cd `dirname $BASH_SOURCE` && pwd)

cd $SH/manifests-mysql

kubectl delete -f mysql.yml

cd $SH/manifests-wordpress

kubectl delete -f wordpress.yml

