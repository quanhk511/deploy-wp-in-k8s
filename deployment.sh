#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

kubectl apply -f "$SH/manifests-mysql/mysql.yml"
    l="app=mysql" kw='AFTERMATH=mysql'; source "$SH/wait4ready.sh"

kubectl apply -f "$SH/manifests-wordpress/wordpress.yml"
    l="app=wordpress" kw='AFTERMATH=wordpress'; source "$SH/wait4ready.sh"

echo
source "$SH/aftermath.sh"
